const express = require('express');
const app = express();
const cors = require('cors');
app.use(express.json());
app.use(express.urlencoded({extended:false}));

app.use(cors());
app.use(require('./routes/index'));

app.listen(4000);
console.log('Server on port 4000');