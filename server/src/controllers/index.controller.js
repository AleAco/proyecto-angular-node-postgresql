const { Pool } = require('pg');

const pool = new Pool({
    host: 'localhost',
    user: 'postgres',
    password: '',
    database: 'firstapi',
    port: '5432'
})

const getUsers = async (req, res) => {
    const response = await pool.query('SELECT * FROM users');
    res.status(200).json(response.rows);
};

const getUserById = async (req, res) => {
    const id = req.params.id;
    const response = await pool.query('SELECT * FROM users WHERE id = $1', [id]);
    res.status(200).json(response.rows);
};

const postUsers = async (req, res) => {
    const { name, email, position, office, salary } = req.body;
    await pool.query('INSERT INTO users (name, email, position, office, salary) VALUES ($1, $2, $3, $4, $5)',
        [name, email, position, office, salary]);
    res.status(200).json({
        message: 'User Added Succesfully',
        body: {
            user: {name, email, position, office, salary}
        }
    });
};

const deleteUser = async (req, res) => {
    const id = req.params.id;
    await pool.query('DELETE FROM users WHERE id = $1', [id]);
    res.status(200).json(`User ${id} deleted successfully`);
};

const updateUser = async (req, res) => {
    const { name, email, position, office, salary } = req.body;
    const id = req.params.id;
    await pool.query('UPDATE users SET name = $1, email = $2, position = $3, office = $4, salary = $5 WHERE id = $6', [
        name,
        email,
        position,
        office,
        salary,
        id
    ]);
    res.status(200).json({
        message: 'User Updated Succesfully',
        body: {
            user: {name, email, position, office, salary, id}
        }
    });
};

module.exports = {
    getUsers,
    getUserById,
    postUsers,
    deleteUser,
    updateUser
}