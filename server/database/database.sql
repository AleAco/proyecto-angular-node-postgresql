CREATE DATABASE firstapi;

CREATE TABLE users(
    id SERIAL PRIMARY KEY,
    name VARCHAR(40),
    email TEXT,
    position VARCHAR(50),
    office VARCHAR(50),
    salary INTEGER
);

INSERT INTO users (name, email, position, office, salary) VALUES
    ('ale', 'ale@gmail.com', 'developer back', 'cordoba', '100'),
    ('cele', 'cele@hotmail.com', 'developer front', 'cordoba', '80');