import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";

import { Employee } from "../models/employee";

@Injectable({
  providedIn: "root",
})
export class EmployeeService {
  selectedEmployee: Employee = {
    name: '',
    office: '',
    position: '',
    email: '',
    salary: 0
  };
  employees: Employee[];

  readonly URL_API = "http://localhost:4000/users";

  constructor(private http: HttpClient) {
  }

  postEmployee(employee: Employee) {
    return this.http.post(this.URL_API, employee);
  }

  getEmployees() {
    return this.http.get<Employee[]>(this.URL_API);
  }

  putEmployee(employee: Employee) {
    return this.http.put(this.URL_API + `/${employee.id}`, employee);
  }

  deleteEmployee(id: string) {
    return this.http.delete(this.URL_API + `/${id}`);
  }
}
