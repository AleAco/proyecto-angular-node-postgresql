export class Employee {
  constructor(id = "", name = "", email: "", position = "", office = "", salary = 0) {
    this.id = id;
    this.name = name;
    this.email = email;
    this.position = position;
    this.office = office;
    this.salary = salary;
  }

  id?: string;
  name: string;
  email: string;
  position: string;
  office: string;
  salary: number;
}
