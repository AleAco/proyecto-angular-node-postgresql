# Empleados

Durante la realización de este proyecto, se creo un CRUD (Create, Read, Update, Delete) completo utilizando Angular, Express, Node y PostgreSQL.

Para ejecutar el ejemplo, basta correr desde el lado del **cliente**:
`ng serve`

Desde el **servidor**, ejecutar:
`npm run dev`
